package br.com.eficatus.eficatus.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import br.com.eficatus.eficatus.R;


public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Configurando a Toolbar
        setUpToolbar();

    }

    public void mail(View view) {

        int id = view.getId();
        String to = "";

        if (id == R.id.ic_mail) {
            to = getString(R.string.email);
        } else if (id == R.id.ic_dev) {
            to = getString(R.string.email_dev);
        }

        String subject = getString(R.string.emailAssunto);

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
        email.putExtra(Intent.EXTRA_SUBJECT, subject);

        // need this to prompts email client only
        email.setType("message/rfc822");

        startActivity(Intent.createChooser(email, "Enviar e-mail com:"));

    }

    public void go(View view) {

        int id = view.getId();
        String url = "";

        if (id == R.id.btnSite) {
            url = getString(R.string.urlSite);
        } else if (id == R.id.btnFace) {
            url = getString(R.string.urlFace);
        } else if (id == R.id.btnYT) {
            url = getString(R.string.urlYT);
        }

        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    public void login(View view) {
        Intent intent = new Intent();
        intent.setClass(MainActivity.this, LoginActivity.class);
        startActivity(intent);
    }
}
