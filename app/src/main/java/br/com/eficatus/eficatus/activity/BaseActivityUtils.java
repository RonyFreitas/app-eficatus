package br.com.eficatus.eficatus.activity;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import br.com.eficatus.eficatus.utils.AlertUtils;


/**
 * Created by Rony Freitas on 20/09/2015.
 */
public class BaseActivityUtils extends DebugActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();

    protected Context getContext() {
        return this;
    }

    protected Activity getActivity() {
        return this;
    }

    protected void log(String msg) {
        Log.d(TAG, msg);
    }

    protected void toast(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    protected void toast(int msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    protected void alert(String msg) {
        AlertUtils.alert(this, msg);
    }

    protected void alert(String title, String msg) {
        AlertUtils.alert(this, title, msg);
    }

    protected void alert(int msg) {
        AlertUtils.alert(this, getString(msg));
    }

    protected void alert(int title, int msg) {
        AlertUtils.alert(this, getString(title), getString(msg));
    }

}
