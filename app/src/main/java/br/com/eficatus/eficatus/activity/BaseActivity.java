package br.com.eficatus.eficatus.activity;

import android.support.v7.widget.Toolbar;

import br.com.eficatus.eficatus.R;

/**
 * Created by Rony Freitas on 20/09/2015.
 */
public class BaseActivity extends BaseActivityUtils {

    /**
     * Configura a toolbar em uma Activity
     */
    protected void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setLogo(R.mipmap.ic_launcher);
        }
    }
}
