package br.com.eficatus.eficatus;

import android.app.Application;
import android.util.Log;

/**
 * Created by rony on 21/11/15.
 */
public class App extends Application {
    private static final String TAG = "Eficatus";
    private static App instance = null;

    public static App getInstance() {
        return instance; // Singleton
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "App.onCreate()");
        instance = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.d(TAG, "App.onTerminate()");
    }

}
