package br.com.eficatus.eficatus.ws;

import android.os.StrictMode;
import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import br.com.eficatus.eficatus.model.User;

/**
 * Created by root on 29/06/17.
 */

public class WS {

    public static String login(User u) {
        String NAMESPACE = "http://eficatus.com.br/e-fica";
        String METODO = "Mobile_Login";

        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        SoapObject soapObject = new SoapObject(NAMESPACE, METODO);
        soapObject.addProperty("CNPJ", u.cnpj);
        soapObject.addProperty("Usuario", u.userName);
        soapObject.addProperty("Senha", u.senha);
        soapObject.addProperty("srChaveSeguranca", u.key);

        SoapSerializationEnvelope soapSerializationEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
        soapSerializationEnvelope.dotNet = true;
        soapSerializationEnvelope.implicitTypes = true;
        soapSerializationEnvelope.setOutputSoapObject(soapObject);

        HttpTransportSE httpTransportSE = new HttpTransportSE("http://eficatus.com.br/site/ws/e-fica.asmx?WSDL");

        try {
            httpTransportSE.call(NAMESPACE + METODO, soapSerializationEnvelope);

            return  soapSerializationEnvelope.getResponse().toString();

        } catch (Exception e) {
            e.printStackTrace();
            return "Ocorreu um erro na conexão com WS.";
        }

    }
}
