package br.com.eficatus.eficatus.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import br.com.eficatus.eficatus.R;
import br.com.eficatus.eficatus.model.User;
import br.com.eficatus.eficatus.ws.WS;

public class LoginActivity extends AppCompatActivity implements Runnable {

    private Handler handler = new Handler();
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        textView = (TextView) findViewById(R.id.result);
    }

    public void go(View v){
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {

        User user = new User("23.105.319/0001-87", "teste", "teste", "Rony");

        final String result = WS.login(user);
        Log.i("WSWSWS",  result);

        handler.post(new Runnable() {
            @Override
            public void run() {
                textView.setText(result);
            }
        });
    }
}
