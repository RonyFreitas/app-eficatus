package br.com.eficatus.eficatus.model;

/**
 * Created by root on 29/06/17.
 */

public class User {

    public String cnpj;
    public String userName;
    public String senha;
    public String key;

    public User(String cnpj, String userName, String senha, String key) {
        this.cnpj = cnpj;
        this.userName = userName;
        this.senha = senha;
        this.key = key;
    }
}
